"""
This file contains all functionality to request data from the gw2 api endpoint.
"""

import requests
import re

_api_schema_version = '2019-02-21T00:00:00Z' # Can also be set to 'latest'
_valid_endpoints = {'account', 'worlds', 'guild', 'colors'}
_request_url = 'https://api.guildwars2.com/v2/' # TODO change to dict with _valid_endpoints


def request(endpoint: str, payload: dict = {}):
    # TODO check if payload differs based on the endpoint
    # Check if the request is to a valid endpoint
    if re.findall("[\w]+", endpoint)[0] not in _valid_endpoints:
        raise Exception("Invalid request endpoint")

    payload['v'] = _api_schema_version
    r = requests.get(_request_url + endpoint, payload)
    
    # 401 is unauthorized, but is returned when API key is invalid.
    if r.status_code == 401:
        raise Exception("Invalid API key, returning...\n")

    # Check if any other status codes has been recieved
    if not r.status_code == 200:
        raise Exception("HTTP status code {}".format(r.status_code))

    data = r.json()

    # Check if the json contains correct data
    # TODO check if 'text' appears in all wrong requests
    if 'text' in data:
        raise Exception(data['text'])

    return data