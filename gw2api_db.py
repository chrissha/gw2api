import os
import sqlite3
from gw2api_request import request
from typing import Tuple

connection = None
cursor = None

def create_tables() -> None:
	# Create account table
	stmt = '''
			CREATE TABLE IF NOT EXISTS accounts (
			  	id 				text NOT NULL UNIQUE,
				apikey 			text NOT NULL UNIQUE,
				request_time 	text NOT NULL,
				name 			text NOT NULL UNIQUE,
				age 			integer NOT NULL,
				last_modified 	text NOT NULL,
				world 			text NOT NULL,
				created 		text NOT NULL,
				commander 		text NOT NULL,
				guilds			text,
				guild_leader	text,
				access			text NOT NULL,
				daily_ap		integer NOT NULL,
				monthly_ap		integer NOT NULL,
				fractal_level	integer NOT NULL,
				wvw_rank		integer NOT NULL
			  );

			CREATE TABLE IF NOT EXISTS worlds (
				id			integer NOT NULL UNIQUE,
				name		text NOT NULL UNIQUE,
				population	text NOT NULL
			);

			CREATE TABLE IF NOT EXISTS guilds (
				id				text NOT NULL UNIQUE,
				name			text NOT NULL,
				tag				text NOT NULL,
				level			integer NOT NULL,
				motd			text NOT NULL,
				influence		integer NOT NULL,
				aetherium		integer NOT NULL,
				resonance		integer NOT NULL,
				favor			integer NOT NULL,
				member_count	integer NOT NULL,
				member_capacity	integer NOT NULL
				/*emblem probably needs own table*/
			);

			/* Simple table for storing names of all dyes */
			CREATE TABLE IF NOT EXISTS dyes (
				id		integer NOT NULL,
				name	text NOT NULL
			)
	'''

	try:
		cursor.executescript(stmt)
	except sqlite3.OperationalError:
		print("Could not create tables")


def account_exists(id: str) -> bool:
	"""
	Checks if an account with the given id already exists in the database.
	
	params:
		id: string - The unique ID of the gw2 account.
	returns:
		bool - True if it exists, False otherwise.
	"""

	try:
		r = cursor.execute("SELECT * FROM accounts WHERE id = ?", (id,))
		if r.fetchone() != None:
			return True
	except sqlite3.Error as e:
		print("An error occured checking if an account exists")
		print(e)
	return False


def save_account(account: object) -> None:
	"""
	Saves an account to the database

	params:
		account: object - A Gw2_account object containing account data
	"""

	if account_exists(account.id):
		print("Account already in database\n")
		return

	try:
		with connection:
			cursor.execute("INSERT INTO accounts VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			(
				account.id,
				account.apikey,
				account.request_time,
				account.name,
				account.age,
				account.last_modified,
				account.world,
				account.created,
				account.commander,
				','.join(account.guilds),		# List into single string
				','.join(account.guild_leader),
				','.join(account.access),
				account.daily_ap,
				account.monthly_ap,
				account.fractal_level,
				account.wvw_rank
			))
	except sqlite3.OperationalError as e:
		raise Exception(e)


def retrieve_account(name: str) -> object:
	try:
		cursor.execute("SELECT * FROM accounts WHERE name LIKE '%" + name + "%'")
	except sqlite3.Error as e:
		print("Error retrieving account\n", e)
		return None
	
	return cursor.fetchone()
	
	

def delete_account(id: str) -> None:
	try:
		with connection:
			cursor.execute("DELETE FROM accounts WHERE id = ?", (id,))
	except sqlite3.Error as e:
		print("Deletion failed.\n", e)


def add_worlds():
	"""
	Retrieves all worlds and adds them to the database. Checks if
	worlds are already in database before a request is sendt to the 
	API. Exits the program if an error occurs.
	"""
	try:
		with connection:
			r = cursor.execute("SELECT COUNT(id) FROM worlds")
			if r.fetchone()[0] > 0:
				return 
	except sqlite3.Error as e:
		print("SQL error. {}".format(e))

	# If no worlds are in database we retrieve all
	worlds = request('worlds', {'ids': 'all'})
	try:
		for world in worlds:
			cursor.execute("INSERT INTO worlds VALUES (?, ?, ?)", 
			(world['id'], world['name'], world['population']))
	except sqlite3.Error as e:
		print("Could not insert worlds. {}".format(e))
		exit()
	connection.commit()


def get_world_info(id: int) -> Tuple[str, str]:
	"""
	Returns information about a world.

	params:
		id: int - The world id.
	returns:
		tuple(str, str) - A tuple with world name and population 
						  respectively.
	"""
	try:
		r = cursor.execute("SELECT name, population FROM worlds WHERE id={}".format(id))
		return r.fetchone()
	except sqlite3.Error as e:
		print("Error when recieving world info. {}".format(e))
	return


def add_guild(guild: object):
	"""
	Adds a guild represented by the Guild object to the database.

	params:
		guild: object - A Guild object to add to the database
	"""
	try:
		with connection:
			cursor.execute("INSERT INTO guilds VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			(
				guild.id,
				guild.name,
				guild.tag,
				guild.level,
				guild.motd,
				guild.influence,
				guild.aetherium,
				guild.resonance,
				guild.favor,
				guild.member_count,
				guild.member_capacity
			))
	except sqlite3.Error as e:
		raise print("Could not insert guild into database.\n{}".format(e))


def get_guild_by_id(id: str) -> object:
	"""
	Searches and returns a row from the database with the guild given by name. Returns
	nothing if no guild was found.

	params:
		name: str - Whole or parts of the name of a guild.
	returns:
		The whole row with the guild info from the database as a tuple.
	"""	
	try:
		r = cursor.execute("SELECT * from guilds WHERE id LIKE '%" + id + "%'")
		row = r.fetchone()
		if row is not None:
			return row
	except sqlite3.Error as e:
		print("Could not retrieve guild.\n{}".format(e))


def get_guild_by_name(name: str) -> object:
	"""
	Searches and returns a row from the database with the guild given by name. Returns
	nothing if no guild was found.

	params:
		name: str - Whole or parts of the name of a guild.
	returns:
		The whole row with the guild info from the database as a tuple.
	"""	
	try:
		r = cursor.execute("SELECT * from guilds WHERE name LIKE '%" + name + "%'")
		row = r.fetchone()
		if row is not None:
			return row
	except sqlite3.Error as e:
		print("Could not retrieve guild.\n{}".format(e))


def fill_dye_table():
	"""
	Fills the dye table with all dyes and their corresponding name.
	Checks if the table is filled before requesting.
	"""
	try:
		r = cursor.execute("SELECT * FROM dyes")
		if r.fetchone() is not None: return
	except sqlite3.Error as e:
		print(e)
		return

	# Request all dyes
	ids = request('colors')
	id_str = ','.join([str(i) for i in ids])
	dyes = request('colors?ids=' + id_str)
	dye_list = []
	for dye in dyes:
		dye_list.append((dye['id'], dye['name']))

	# Insert dyes into database
	try:
		for dye_id, dye_name in dye_list:
			cursor.execute("INSERT INTO dyes VALUES (?, ?)", 
			(dye_id, dye_name))
	except sqlite3.Error as e:
		print("Could not insert dyes. {}".format(e))
		exit()
	connection.commit()


def init():
	global connection, cursor
	connection = sqlite3.connect('gw2.db')
	cursor = connection.cursor()

	create_tables()
	add_worlds()
	fill_dye_table()

def close():
	global connection, cursor
	cursor.close()
	connection.close()
