import time
import gw2api_db as db
from gw2api_request import request

class Account:
    """
    Base constructor for the Account class, constructs a 
    Account object from a dictionary. For other constructors see 
    this class' @classmethods.
    
    Notes: guilds is a list containing 0-5 elements.

    params: 
        data: dict - A dictionary containing account data.
    """
    def __init__(self, data: dict):
        self.access         = data['access'] 
        self.age            = data['age'] 
        self.apikey         = data['apikey'] 
        self.commander      = data['commander'] 
        self.created        = data['created'] 
        self.daily_ap       = data['daily_ap'] 
        self.fractal_level  = data['fractal_level'] 
        self.guild_leader   = data['guild_leader'] 
        self.guilds         = data['guilds'] 
        self.id             = data['id'] 
        self.last_modified  = data['last_modified'] 
        self.monthly_ap     = data['monthly_ap']
        self.name           = data['name']
        self.request_time   = data['request_time']
        self.world          = data['world']
        self.wvw_rank       = data['wvw_rank']


    """
    Creates an Account object from a json file. Does not check whether
    the json file actually contains the required data. Currently redundant.

    params:
        json: dict - A json file containing account data.
    """
    @classmethod
    def from_json(cls, json: dict) -> object:
        return cls(json)


    """
    Sends a request to the gw2 API using the given api key. Constructs
    an Account object from the requested information. If the api key
    is incorrect, the program will exit with an error message.

    params:
        apikey: str - A Guild Wars 2 account API key as a string with 
                      full access to everything. 
    """
    @classmethod
    def from_api(cls, apikey: str) -> object:
        payload = {'access_token': apikey}
        data    = request('account', payload)
        data['apikey'] = apikey
        data['request_time'] = time.time()
        return cls.from_json(data)
        

    """
    Creates an Account object from a db SELECT statement.

    params:
        db_entry: tuple - A single entry from the accounts database.
    """
    @classmethod
    def from_db_entry(cls, db_entry: tuple) -> object:
        d = {
                'id':               db_entry[0], 
                'apikey':           db_entry[1], 
                'request_time':     db_entry[2],
                'name':             db_entry[3], 
                'age':              db_entry[4], 
                'last_modified':    db_entry[5],
                'world':            db_entry[6], 
                'created':          db_entry[7], 
                'commander':        db_entry[8],
                'guilds':           db_entry[9].split(','), 
                'guild_leader':     db_entry[10].split(','), 
                'access':           db_entry[11].split(','), 
                'daily_ap':         db_entry[12], 
                'monthly_ap':       db_entry[13], 
                'fractal_level':    db_entry[14], 
                'wvw_rank':         db_entry[15]
            }
        return cls(d)


    def __repr__(self):
        acc_str = "\n"

        acc_creation_date       = time.strptime(self.created, "%Y-%m-%dT%H:%M:%S%z")
        acc_age_hours           = int(self.age // 3600)
        acc_age_min             = int(self.age % 60)
        world_name, world_pop   = db.get_world_info(self.world)
        world_lang              = {0: 'EN', 1: 'FR', 2: 'DE', 3: 'ES'}[int(str(self.world)[1])]

        acc_str += "Account name: "     + self.name + "\n"
        acc_str += "Creation date: "    + time.strftime("%d. %b %Y", acc_creation_date) + "\n"
        acc_str += "Account age: "      + str(acc_age_hours) + " hours " + str(acc_age_min) + " minutes" + "\n"

        acc_str += "World: "            + world_name + " "
        acc_str += "(Region: "          + ("Europe" if int(str(self.world)[0]) == 2 else "North America") + ", "
        acc_str += "Population: "       + world_pop + ", "
        acc_str += "Language: "         + world_lang + ")\n"

        acc_str += "Daily ap: "         + str(self.daily_ap) + "\n"
        acc_str += "Monthly ap: "       + str(self.monthly_ap) + "\n"
        acc_str += "Fractal level: "    + str(self.fractal_level) + "\n"
        acc_str += "Commander: "        + ("Yes" if self.commander == 1 else "No") + "\n"
        acc_str += "WvW rank: "         + str(self.wvw_rank) + "\n\n"

        # TODO add guild names instead of guild id
        acc_str += "Guilds:\n"
        for guild_id, guild_leader_id in zip(self.guilds, self.guild_leader):
            guild = Guild.from_db_entry(db.get_guild_by_id(guild_id))
            acc_str += "\t" + "{} [{}]".format(guild.name, guild.tag)
            acc_str += " (Leader)\n" if guild_id == guild_leader_id else "\n"

        acc_str += "Access:\n"
        for access in self.access:
            acc_str += "\t" + access + "\n"

        return acc_str


class Guild:
    """
    Base constructor for the Guild class.

    params:           
        data: dict - Dictionary with every entry. Does not check
                     whether the dict has the required fields.
    """
    def __init__(self, data: dict):
        self.id                 = data['id']
        self.name               = data['name']
        self.tag                = data['tag']
        self.level              = data['level']
        self.motd               = data['motd']
        self.influence          = data['influence']
        self.aetherium          = data['aetherium']
        self.resonance          = data['resonance']
        self.favor              = data['favor']
        self.member_count       = data['member_count']
        self.member_capacity    = data['member_capacity']

    """
    Creates a guild object from a json object.

    params:
        json: dict - A json object containing guild data. Usually from
                     the API.
    """
    @classmethod
    def from_json(cls, json: dict) -> object:
        return cls(json)

    """
    Creates a Guild object from a raw database entry tuple.

    params:
        db_entry: tuple - Tuple from a database row in the guilds table
    """
    @classmethod
    def from_db_entry(cls, db_entry: tuple) -> object:
        d = {
                'id':               db_entry[0], 
                'name':             db_entry[1], 
                'tag':              db_entry[2],
                'level':            db_entry[3], 
                'motd':             db_entry[4], 
                'influence':        db_entry[5],
                'aetherium':        db_entry[6], 
                'resonance':        db_entry[7], 
                'favor':            db_entry[8],
                'member_count':     db_entry[9], 
                'member_capacity':  db_entry[10]
            }
        return cls(d)


    def __repr__(self):
        g_str = ""

        g_str += "Guild: {} [{}]\n".format(self.name, self.tag)
        g_str += "Level: {}\n".format(self.level)
        g_str += "Members: {}/{}\n".format(self.member_count, self.member_capacity)
        g_str += "Message of the day: {}\n".format(self.motd)
        g_str += "Influence: {}\n".format(self.influence)
        g_str += "Aetherium: {}\n".format(self.aetherium)
        g_str += "Resonance: {}\n".format(self.resonance)
        g_str += "Favor: {}\n".format(self.favor)
        return g_str