"""
The main file to start when running the gw2api project.
Contains mostly code for interaction between the user and the program.
"""

# Python standard library
import os
import re

# External libraries
from PyInquirer import prompt, Separator, Validator, ValidationError

# Local
from gw2api_classes import Account, Guild
from gw2api_request import request
import gw2api_db as db


main_menu = [
    {
        'type': 'list',
        'name': 'main_menu',
        'message': 'Choose an option',
        'choices': [
            'Account',
            'Guild',
            'Dye',
            Separator(),
            'Exit'
        ]
    }
]

# PyInquirer question for options regarding accounts
menu_account_options = [
    {
        'type': 'list',
        'name': 'account_menu',
        'message': 'Guild Wars 2 account options:',
        'choices': [
            'Add account',
            'View account',
            'Delete account',
            Separator(),
            'Back',
            'Exit'
        ]
    }
]

# PyInquirer question for adding accounts
menu_add_account = [
    {
        'type': 'input',
        'name': 'add_account',
        'message': 'Add Guild Wars 2 API key',
    }
]

menu_search_account = [
    {
        'type': 'input',
        'name': 'search_account',
        'message': 'Search for Guild Wars 2 account name'
    }
]

menu_delete_account = [
    {
        'type': 'input',
        'name': 'delete_account',
        'message': 'Search for name of account to delete'
    }
]

delete_account_confirm = [
    {
        'type': 'confirm',
        'name': 'confirm_delete',
        'message': 'Are you sure you want to delete this account?',
        'default': False
    }
]

menu_guild_options = [
    {
        'type': 'list',
        'name': 'guild_menu',
        'message': 'Guild options:',
        'choices': [
            'Search guild',
            Separator(),
            'Back',
            'Exit'
        ]
    }
]

menu_guild_search = [
    {
        'type': 'input',
        'name': 'search_guild',
        'message': 'Search for guild by guild name'
    }
]

menu_dye_search = [
    {
        'type': 'input',
        'name': 'search_dye',
        'message': 'Type in dye name'
    }
]


def add_account() -> None:
    """
    Prompts the user to add an account using PyInquirer, if the user 
    does not provide an API key, this function simply returns. Other 
    exceptions are handled when requesting the account and adding it to
    the database. Assumes that the API key includes the progression scope.
    """
    answer = prompt(menu_add_account)
    apikey = answer['add_account']
    if len(apikey) == 0:
        print("No API key recieved.\n")
        return
    try:
        acc = Account.from_api(apikey)
        db.save_account(acc)

    except Exception as e:
        print("Could not add account. {}".format(e))

    # Add the guilds associated with the account
    for guild_id in acc.guilds:
        guild = db.get_guild_by_id(guild_id)
        if guild is not None: continue  # Guild already in db
        data = request('guild/' + guild_id, {'access_token': acc.apikey})
        guild = Guild.from_json(data)
        db.add_guild(guild)

    print("Account added\n")

def view_account():
    """
    Lets the user search for an account by account name. The first
    result found is made into a Gw2_account object and printed out.
    """
    # TODO add show all names or search
    answer = prompt(menu_search_account)
    acc_name = answer['search_account'].strip().lower().replace(' ', '')
    if len(acc_name) == 0:
        print("No name recieved.\n")
        return

    acc_dat = db.retrieve_account(acc_name)
    if acc_dat == None:
        print("No account found.\n")
        return
    acc = Account.from_db_entry(acc_dat)
    
    # TODO add ability to pick account if multiple were found
    print("Found account {}".format(acc.name))
    print(acc)


def delete_account() -> None:
    answer = prompt(menu_delete_account)
    acc_name = answer['delete_account'].strip().lower().replace(' ', '')
    if len(acc_name) == 0:
        print("No name recieved.\n")
        return

    acc = db.retrieve_account(acc_name)
    if acc == None:
        print("No account found.\n")
        return

    acc = Account.from_db_entry(acc)
    print("Found account {}".format(acc.name))
    answer = prompt(delete_account_confirm)
    if answer['confirm_delete']:
        db.delete_account(acc.id)
        print("{} has been removed.".format(acc.name))
        return
    print("Account deletion cancelled.")


def search_guild_by_name():
    """
    Searches for a guild and prints information about that guild.
    Searches locally first for the guild, then from the API.
    If only parts of the guild name is supplied the program will
    only be able to find the guild if it is stored in the database.
    """
    guild_name = prompt(menu_guild_search)['search_guild']
    guild_name = guild_name[0:31].strip()

    if len(guild_name) == 0:
        print("No guild name recieved.")
        return
    
    # Check if guild exists in the database
    guild = db.get_guild_by_name(guild_name)
    if guild is not None:
        guild = Guild.from_db_entry(guild)
        print("Found guild {} [{}]\n".format(guild.name, guild.tag))
        print(guild)
        return
    else:
        print("No guild found locally.\n")


def search_dye():
    # Search dye did not work very well..
    #answer = prompt(menu_dye_search)['search_dye']
    


def clear():
    # Clears the console, supports Windows and Linux
    os.system('cls' if os.name == 'nt' else 'clear')


if __name__ == "__main__":
    print("Initializing")
    db.init()

    clear()
    print("Welcome to Guild Wars 2 helper\n")
    _state = "MAIN_MENU"
 
    # Run endless loop until exit is pressed
    while (True):
        if _state == "MAIN_MENU":
            answer = prompt(main_menu)['main_menu']
            if answer == 'Account':
                _state = "ACCOUNT_MENU"
            elif answer == 'Guild':
                _state = "GUILD_MENU"
            elif answer == 'Dye':
                search_dye()
            elif answer == 'Exit':
                break
            else:
                pass

        elif _state == "ACCOUNT_MENU":
            answer = prompt(menu_account_options)['account_menu']
            if answer == 'Add account':
                add_account()
            elif answer == 'View account':
                view_account()
            elif answer == 'Delete account':
                delete_account()
            elif answer == 'Back':
                _state = "MAIN_MENU"
            elif answer == 'Exit':
                break
            else:
                _state = "MAIN_MENU"
        
        elif _state == "GUILD_MENU":
            answer = prompt(menu_guild_options)['guild_menu']
            if answer == 'Search guild':
                search_guild_by_name()
            elif answer == 'Back':
                _state = "MAIN_MENU"
            elif answer == 'Exit':
                break
            else:
                _state = "MAIN_MENU"

        # If state is unknown we report the error and quit
        else:
            print("Unrecognized state")
            break
    
    db.close()